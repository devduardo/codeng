import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/site/home/home.component';
import { NosotrosComponent } from './components/site/nosotros/nosotros.component';
import { ServiciosComponent } from './components/site/servicios/servicios.component';
import { ProductsComponent } from './components/site/products/products.component';
import { ContactoComponent } from './components/site/contacto/contacto.component';

const routes: Routes = [
  { path: '', redirectTo:'home', pathMatch:'full'},
  { path: 'home', component: HomeComponent},
  { path: 'nosotros', component: NosotrosComponent},
  { path: 'servicios', component: ServiciosComponent},
  { path: 'productos', component: ProductsComponent},
  { path: 'contactos', component: ContactoComponent},


];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
