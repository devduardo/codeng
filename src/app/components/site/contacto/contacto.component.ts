import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from  '@angular/forms';
import { ContactoService } from 'src/app/contacto.service';

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.css']
})
export class ContactoComponent implements OnInit {
  contactForm: FormGroup;
  touched: boolean;
  name: any;
  valid: boolean;
  surName: any;
  untouched: boolean;
  verificacionForm: any;

  constructor(
    private formBuilder: FormBuilder,
    private contacto: ContactoService
  ) {
    this.createContactForm();
  }

  ngOnInit() {
  }

  createContactForm(){
    this.contactForm = this.formBuilder.group({
      nombres: ['', [Validators.required, Validators.maxLength(30), Validators.pattern(/^[a-zA-Z ]*$/)]],
      apellidos: ['', [Validators.required, Validators.maxLength(30), Validators.pattern(/^[a-zA-Z ]*$/)]],
      correo: ['', [Validators.required, Validators.email, Validators.maxLength(30), Validators.pattern(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/)]],
      mensaje: ['', [Validators.required, Validators.maxLength(300)]],
    });
  }
  public get nombres() {
    return this.contactForm.get('nombres')
  }
  public get apellidos() {
    return this.contactForm.get('apellidos')
  }
  public get correo() {
    return this.contactForm.get('correo')
  }
  public get mensaje() {
    return this.contactForm.get('mensaje')
  }

  onSubmit() {
    this.contacto.submitContacto(this.contactForm.value).then(() => {
      this.contactForm.reset();
    }).catch(() => {
      console.log('Error sending message!')
    });
  }

  scroll(el: HTMLElement) {
    el.scrollIntoView({behavior: 'smooth'});
  }
}
