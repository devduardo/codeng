import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class ContactoService {

  constructor(
    private firestore: AngularFirestore
  ) { }

  submitContacto(data: {nombres, apellidos, correo, mensaje}): Promise<any> {
    return this.firestore.collection('mensajes').add(data);
  }
}
