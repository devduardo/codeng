import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/layout/header/header.component';
import { MenuComponent } from './components/layout/menu/menu.component';
import { FooterComponent } from './components/layout/footer/footer.component';
import { HomeComponent } from './components/site/home/home.component';
import { BackToTopComponent } from './components/elements/back-to-top/back-to-top.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NosotrosComponent } from './components/site/nosotros/nosotros.component';
import { ServiciosComponent } from './components/site/servicios/servicios.component';
import { ContactoComponent } from './components/site/contacto/contacto.component';
import { ProductsComponent } from './components/site/products/products.component';
import { RouterLinkDelayModule } from '@bcodes/ngx-routerlink-delay';
import { ScrollGalleryModule } from '@silicic/scroll-gallery-ng'
import  {  FormsModule,  ReactiveFormsModule  }  from  '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment';
import { AngularFirestore } from '@angular/fire/firestore';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MenuComponent,
    FooterComponent,
    HomeComponent,
    BackToTopComponent,
    NosotrosComponent,
    ServiciosComponent,
    ContactoComponent,
    ProductsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterLinkDelayModule,
    BrowserAnimationsModule,
    ScrollGalleryModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebase)
  ],
  providers: [AngularFirestore, {provide: 'googleTagManagerId', useValue: 'GTM-PF44TQJ'}],
  bootstrap: [AppComponent]
})
export class AppModule { }
